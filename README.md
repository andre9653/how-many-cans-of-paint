# DR'Paints

## Bem vindo ao DR'Paints

Nesse projeto, você pode calcular a quantidade necessária de tinta para pintar um comodo, baseado no m².

## Sumario

## Desenvolvimento

Nessa aplicação foram utilizadas boas praticas de desenvolvimento, como:
- [Conventional Commit](https://conventionalcommits.org/)
- [Mobile First](https://medium.com/@Vincentxia77/what-is-mobile-first-design-why-its-important-how-to-make-it-7d3cf2e29d00)
- [Solid Principles](https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)
- [Metodologias Ágeis](https://sambatech.com/blog/insights/metodos-ageis/)
- [Unique Responsibility Principle](https://www.toptal.com/software/single-responsibility-principle)

<details>
<summary>Regras de negócio</summary>

1. Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes
2. O total de área das portas e janelas deve ser no máximo 50% da área de parede
3. A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
4. Cada janela possui as medidas: 2,00 x 1,20 mtos
5. Cada porta possui as medidas: 0,80 x 1,90
6. Cada litro de tinta é capaz de pintar 5 metros quadrados
7. Não considerar teto nem piso.
8. As variações de tamanho das latas de tinta são:
      - 0,5 L
      - 2,5 L
      - 3,6 L
      - 18 L
</details>

### Tecnologias


- [React](https://reactjs.org/)
- [React-Router](https://reacttraining.com/react-router/)
- [React-Router-Dom](https://reacttraining.com/react-router/web/api/BrowserRouter)
- [React-Bootstrap](https://react-bootstrap.github.io/)
- [React-Testing-Library](https://testing-library.com/docs/react-testing-library/intro)
- [PropTypes](https://reactjs.org/docs/typechecking-with-proptypes.html)
- [Jest](https://jestjs.io/)
- [Context](https://reactjs.org/docs/context.html)
- [Heroku](https://www.heroku.com/)




### Maior desafio

Um dos desafios mais difíceis que eu encontrei foi a configuração do `coverage` do `jest`, que estava com vários problemas de compatibilidade com webpack padrão do `react`, e também a criação de componentes responsivos, po sempre faltar algum detalhe, algum caso de uso não testado.

### Deploy

O deploy da aplicação foi feito no `Heroku`, onde eu uso o [Heroku CLI](https://cli.heroku.com/).
Provavelmente o primeiro acesso ao link aplicação vá demorar um pouco, pois o Heroku precisa que o servidor seja iniciado por estar em hibernação.

link: http://calculate-cans.herokuapp.com/

### Como executar a aplicação localmente

Para que a aplicação seja executada localmente é importante ter a versão `^14` do `node` instalada no computador.

#### Na raiz do projeto execute:
```shell
npm install
```
>  Caso não consiga executar o comando, verifique se o `node` está instalado no computador.

#### Execute a aplicação:
```shell
npm start
```
#### Para executar os testes:
```shell
npm test
```
> Pressione `a` para executar todos os testes se necessário.

> Execute o comando `npx jest --coverage` para gerar o relatório de cobertura.

## Agradecimentos
Obrigado pela oportunidade de desenvolver essa aplicação, com ela pude fixar um pouco mais o meu conhecimento sobre React, utilizar novas ferramentas e ganhar mais experiência com o desenvolvimento.

Estou aberto a feedbacks, sugestões e críticas.