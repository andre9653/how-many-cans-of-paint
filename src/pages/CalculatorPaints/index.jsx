import React from 'react';
import { Container } from 'react-bootstrap';
import FormsWall from '../../components/FormsWall';
import NavBar from '../../components/NavBar';
import Provider from '../../context/FormsWallProvider';
import './index.css';

export default function CalculatorPaints() {
  return (
    <>
      <NavBar />
      <Provider>
        <Container className="calc-container">
          <h5 className="description">
            A seguir informe a
            {' '}
            <i>altura</i>
            {' '}
            e
            {' '}
            <i>largura</i>
            {' '}
            em
            {' '}
            <i>metros</i>
            {' '}
            de cada parede e caso tenha portas ou janelas marque a
            seleção!
          </h5>
          <FormsWall />
        </Container>
      </Provider>
    </>
  );
}
