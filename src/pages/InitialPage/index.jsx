import React from 'react';
import { Button } from 'react-bootstrap';
import NavBar from '../../components/NavBar';
import LinkTo from '../../components/LinkTo';
import './index.css';

export default function InitialPage() {
  const styleLinkRedirect = {
    background: 'none',
    color: 'white',
    textDecoration: 'none',
  };
  return (
    <>
      <NavBar />
      <main className="initial-page">
        <h1 className="title-initial-page" name="title-initial-page">
          Que tal calcular a quantidade
          de tinta que você irá utilizar de uma forma rápida e prática?
        </h1>
        <p className="description-initial-page" data-testid="description-initial-page">
          A DR Desenvolveu uma ferramenta que calcula a quantidade de tinta rapidamente
          para você não se preocupar se vai ou não sobrar tinta!!
        </p>
        <p>
          Portas e janelas tem por padrão os seguintes tamanhos:
          {' '}
          <br />
          <i>Porta: 1,90 x 1,20</i>
          <br />
          <i>Janela: 1,20 x 2,00</i>
          <br />
          <br />
          <i>
            *É importante que o m2 da parede seja pelo
            menos 50% superior ao m2 de portas e janelas juntos!*
          </i>
        </p>
        <Button>
          <LinkTo
            path="/calculator"
            textProp="Calcular agora!!"
            styleProp={styleLinkRedirect}
          />
        </Button>
      </main>
    </>
  );
}
