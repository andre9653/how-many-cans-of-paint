import React from 'react';
import { useLocation } from 'react-router-dom';
import LinkTo from '../../components/LinkTo';
import NavBar from '../../components/NavBar';
import { countQuantityCans } from '../../utils/calculatorQntPaints';
import './index.css';

function createListCans(cans) {
  const result = [];
  Object.keys(cans).forEach((key) => {
    result.push(
      <li key={key}>
        {`${cans[key]} latas de ${key}L`.replace('.', ',')}
      </li>,
    );
  });
  return result;
}
export default function ResultPage() {
  const { state } = useLocation();
  return (
    <>
      <NavBar />
      <main className="container">
        <h4>
          É recomendável que você compre as quantidades a baixo de tinta
          {' '}
          <i>DR&apos;Paints</i>
          {' '}
          para pintar todas as paredes informadas.
        </h4>
        <ul>
          {createListCans(countQuantityCans(state.cans))}
        </ul>
        <LinkTo path="/calculator" className="btn btn-primary" textProp="Calcular novamente" />
      </main>
    </>
  );
}
