const calcM2 = (height, width) => height * width;
const subtractM2 = (m2, sizeDor) => m2 - sizeDor;
const validateM2 = (m2) => m2 >= 1 && m2 <= 50;
const validateSizeAgainstM2 = (size, m2) => size <= m2 * (50 / 100);
const validadeSizeDorAgainstHeight = (sizeDor, height) => sizeDor <= height - 0.30;
const windowSize = { height: 1.20, width: 2.00 };
const doorSize = { height: 1.90, width: 1.20 };
const PaintsCanSizes = [18, 3.6, 2.5, 0.5];

const validAll = (height, width, door = false, window = false) => {
  const m2 = calcM2(height, width);
  const sizeDor = door ? calcM2(doorSize.height, doorSize.width) : 0;
  const sizeWindow = window ? calcM2(windowSize.height, windowSize.width) : 0;
  const validSizeWithDoorAndWindow = validateSizeAgainstM2(sizeDor + sizeWindow, m2);
  const m2SubtractDoor = subtractM2(m2, sizeDor);
  const m2SubtractWindow = subtractM2(m2SubtractDoor, sizeWindow);
  const m2Validate = validateM2(m2SubtractWindow);
  const sizeDorValidate = validadeSizeDorAgainstHeight(sizeDor, height);
  return validSizeWithDoorAndWindow && m2Validate && sizeDorValidate;
};

const calcM2DoorAndWindow = (height, width, door = false, window = false) => {
  if (!validAll(height, width, door, window)) {
    return 'Valores inválidos, verifique se o tamanho de portas com janelas são superiores a 50% do tamanho da parede.';
  }
  const m2 = calcM2(height, width);
  const sizeDor = door ? calcM2(doorSize.height, doorSize.width) : 0;
  const sizeWindow = window ? calcM2(windowSize.height, windowSize.width) : 0;
  const m2SubtractDoor = subtractM2(m2, sizeDor);
  const m2SubtractWindow = subtractM2(m2SubtractDoor, sizeWindow);
  return m2SubtractWindow;
};

const calcQntPaints = (m2) => Math.ceil(m2 / 5) || 'valores inválidos';

// returns quantity of cans prioritizing the largest can
const calcQntPaintsByCan = (liters) => {
  const cans = [];
  let qnt = liters;
  while (qnt > 0) {
    // eslint-disable-next-line no-loop-func
    const canSearch = PaintsCanSizes.find((can) => can <= qnt);
    if (!canSearch) {
      cans.push(0.5);
      qnt -= 0.5;
    } else {
      cans.push(canSearch);
      qnt -= canSearch;
    }
  }
  return cans;
};

const countQuantityCans = (cans) => {
  const result = {};
  cans.forEach((can) => {
    if (result[can]) {
      result[can] += 1;
    } else {
      result[can] = 1;
    }
  });
  return result;
};

export {
  calcM2,
  subtractM2,
  validateM2,
  validateSizeAgainstM2,
  validadeSizeDorAgainstHeight,
  validAll,
  calcM2DoorAndWindow,
  calcQntPaints,
  calcQntPaintsByCan,
  countQuantityCans,
};
