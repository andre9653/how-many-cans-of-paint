/** @jest-environment jsdom */
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import InitialPage from '../pages/InitialPage';

describe('1- Testa pagina de apresentação.', () => {
  test('', () => {
    render(
      <BrowserRouter>
        <InitialPage />
      </BrowserRouter>,
    );

    const navElement = screen.getByRole('navigation');
    expect(navElement).toBeInTheDocument();
  });
  test('Testa se a pagina contem um titulo.', () => {
    render(
      <BrowserRouter>
        <InitialPage />
      </BrowserRouter>,
    );
    const headingElement = screen.getByRole('heading', { name: /Que tal calcular/ });

    expect(headingElement).toBeInTheDocument();
  });
  test('Testa se a pagina contem uma descrição.', () => {
    render(
      <BrowserRouter>
        <InitialPage />
      </BrowserRouter>,
    );
    const pElement = screen.getByTestId('description-initial-page');
    expect(pElement).toBeInTheDocument();
  });
  test('Testa se a pagina contem um botão de redirecionamento.', () => {
    render(
      <BrowserRouter>
        <InitialPage />
      </BrowserRouter>,
    );
    const buttonElement = screen.getByRole('button', { name: /Calcular agora!/ });
    expect(buttonElement).toBeInTheDocument();
  });
});
