import {
  calcM2,
  subtractM2,
  validateM2,
  validateSizeAgainstM2,
  validadeSizeDorAgainstHeight,
  validAll,
  calcM2DoorAndWindow,
  calcQntPaints,
  calcQntPaintsByCan,
  countQuantityCans,
} from '../utils/calculatorQntPaints';

describe('3 - Testa funções responsáveis em efetuar o cálculo da quantidade de tinta.', () => {
  test('Testa que é possível calcular o m2', () => {
    expect(calcM2(3, 4)).toBe(12);
    expect(calcM2(10, 20)).not.toBe(100);
  });

  test('Testa que é possível subtrair m2 de um determinado valor.', () => {
    expect(subtractM2(3, 1)).toBe(2);
    expect(subtractM2(3, 1)).not.toBe(1);
  });

  test('Testa que se o m2 for menor que 1 ou maior que 50 retorna "false".', () => {
    expect(validateM2(50)).toBe(true);
    expect(validateM2(1)).toBe(true);
    expect(validateM2(51)).toBe(false);
    expect(validateM2(0)).toBe(false);
  });

  test('Testa que se o tamanho da porta e janela for maior que 50% do tamanho total retorna "false".', () => {
    expect(validateSizeAgainstM2(4.2, 12)).toBe(true);
    expect(validateSizeAgainstM2(4.2, 8)).toBe(false);
  });

  test('Testa que se o tamanho da parede não for pelo menos 30cm maior que a altura da porta retorna "false".', () => {
    expect(validadeSizeDorAgainstHeight(1.90, 3)).toBe(true);
    expect(validadeSizeDorAgainstHeight(1.71, 2)).toBe(false);
    expect(validadeSizeDorAgainstHeight(1.90, 2.20)).toBe(true);
  });

  test('Testa que passado todas as validações necessárias retorna "true".', () => {
    expect(validAll(3, 5, false, false)).toBe(true);
    expect(validAll(3, 4, true, false)).toBe(true);
    expect(validAll(3, 4, false, true)).toBe(true);
    expect(validAll(3, 4, true, true)).toBe(true);
    expect(validAll(3, 3, true, true)).toBe(false);
  });

  test('Testa se é possível calcular m2 de uma parede.', () => {
    expect(calcM2DoorAndWindow(3, 4)).toBe(12);
    expect(calcM2DoorAndWindow(3, 4)).not.toBe(13);
    expect(calcM2DoorAndWindow(3, 4, false, true)).toBe(9.6);
    expect(calcM2DoorAndWindow(3, 4, true, false)).toBe(9.72);
    expect(calcM2DoorAndWindow(3, 4, true, true)).toBe(7.32);
    expect(calcM2DoorAndWindow(3, 3, true, true)).toBe('Valores inválidos, verifique se o tamanho de portas com janelas são superiores a 50% do tamanho da parede.');
    expect(calcM2DoorAndWindow(0, 3, false, false)).toBe('Valores inválidos, verifique se o tamanho de portas com janelas são superiores a 50% do tamanho da parede.');
  });

  test('Testa que retorna quantos litros de tinta é preciso para pintar uma parede.', () => {
    expect(calcQntPaints(12)).toBe(3);
    expect(calcQntPaints(12)).not.toBe(4);
    expect(calcQntPaints(9.6)).toBe(2);
    expect(calcQntPaints(9.72)).toBe(2);
    expect(calcQntPaints(7.32)).toBe(2);
    expect(calcQntPaints(0)).toBe('valores inválidos');
  });

  test('Testa que é possível retornar a quantidade de latas necessária para pintar a parede.', () => {
    expect(calcQntPaintsByCan(3)).toEqual([2.5, 0.5]);
    expect(calcQntPaintsByCan(3)).not.toEqual([2.5, 2.5]);
    expect(calcQntPaintsByCan(2)).toEqual([0.5, 0.5, 0.5, 0.5]);
    expect(calcQntPaintsByCan(2)).not.toBe(2);
    expect(calcQntPaintsByCan(1)).toEqual([0.5, 0.5]);
    expect(calcQntPaintsByCan(10)).toEqual([3.6, 3.6, 2.5, 0.5]);
    expect(calcQntPaintsByCan(18)).toEqual([18]);
    expect(calcQntPaintsByCan(20)).toEqual([18, 0.5, 0.5, 0.5, 0.5]);
  });

  test('Testa que é possível retornar um objeto com as quantidades de latas necessarias', () => {
    expect(countQuantityCans([2.5, 0.5])).toEqual({
      2.5: 1,
      0.5: 1,
    });
    expect(countQuantityCans([2.5, 2.5])).toEqual({
      2.5: 2,
    });
    expect(countQuantityCans([0.5, 0.5, 0.5, 0.5])).toEqual({
      0.5: 4,
    });
    expect(countQuantityCans([3.6, 3.6, 2.5, 0.5])).toEqual({
      3.6: 2,
      2.5: 1,
      0.5: 1,
    });
  });
});
