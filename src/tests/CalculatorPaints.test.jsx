/** @jest-environment jsdom */
import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import { click } from '@testing-library/user-event/dist/click';
import React from 'react';

import { BrowserRouter } from 'react-router-dom';
import CalculatorPaints from '../pages/CalculatorPaints';

describe('2- Testa pagina de cálculo.', () => {
  test('Testa que existe um componente de "NavBar".', () => {
    render(
      <BrowserRouter>
        <CalculatorPaints />
      </BrowserRouter>,
    );

    const navElement = screen.getByRole('navigation');
    expect(navElement).toBeInTheDocument();
  });

  test('Testa que exista componentes que representam as paredes', () => {
    render(
      <BrowserRouter>
        <CalculatorPaints />
      </BrowserRouter>,
    );

    const wallElement = screen.getAllByTestId('wall');
    expect(wallElement[0]).toBeInTheDocument();
    expect(wallElement.length).toBe(4);
  });
  test('Testa que existe um componente do tipo checkbox.', () => {
    render(
      <BrowserRouter>
        <CalculatorPaints />
      </BrowserRouter>,
    );

    const checkboxElement = screen.getAllByTestId('input-checkBox');
    expect(checkboxElement[0]).toBeInTheDocument();
    click(checkboxElement[0]);
    expect(checkboxElement[0].checked).toBe(true);
    expect(checkboxElement.length).toBe(8);
  });
  test('Testa se contem um input para informar os m² da parede.', () => {
    render(
      <BrowserRouter>
        <CalculatorPaints />
      </BrowserRouter>,
    );

    const inputElement = screen.getAllByTestId('input-m2');
    expect(inputElement[0]).toBeInTheDocument();
    expect(inputElement.length).toBe(8);
  });
  test('Testa que existam componentes para ilustrar a parede e caso porta ou janela ilustrar também.', () => {
    render(
      <BrowserRouter>
        <CalculatorPaints />
      </BrowserRouter>,
    );

    const wallElement = screen.getAllByTestId('wall');
    expect(wallElement[0]).toBeInTheDocument();
    const checkboxElement = screen.getAllByTestId('input-checkBox');
    expect(checkboxElement[0]).toBeInTheDocument();
    click(checkboxElement[0]);
    click(checkboxElement[1]);
    const doorElement = screen.getAllByTestId('door');
    const windowElement = screen.getAllByTestId('window');
    expect(doorElement[0]).toBeInTheDocument();
    expect(windowElement[0]).toBeInTheDocument();
  });

  test('Testa que existe um componente para executar o calculo.', async () => {
    render(
      <BrowserRouter>
        <CalculatorPaints />
      </BrowserRouter>,
    );

    const buttonElement = screen.getByTestId('button-submit');
    expect(buttonElement).toBeInTheDocument();
    const inputElements = screen.getAllByTestId('input-m');
    expect(inputElements.length).toBe(8);
    fireEvent.click(buttonElement);
    // implementar futuramente componente de alerta**
    inputElements.forEach((inputElement) => {
      fireEvent.change(inputElement, { target: { value: '4' } });
    });
    click(buttonElement);
  });
});
