import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Routes } from 'react-router-dom';
import InitialPage from './pages/InitialPage';
import CalculatorPaints from './pages/CalculatorPaints';
import ResultPage from './pages/ResultPage';

function App() {
  return (
    <Routes>
      <Route path="/" element={<InitialPage />} />
      <Route path="/calculator" element={<CalculatorPaints />} />
      <Route path="/result" element={<ResultPage />} />
    </Routes>
  );
}

export default App;
