import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import FormsContext from './FormsWallContext';

export default function Provider({ children }) {
  const [forms, setForms] = useState({
    wall1: {
      door: false, window: false, altura: '', largura: '',
    },
    wall2: {
      door: false, window: false, altura: '', largura: '',
    },
    wall3: {
      door: false, window: false, altura: '', largura: '',
    },
    wall4: {
      door: false, window: false, altura: '', largura: '',
    },
  });

  const [cans, setCans] = useState([]);

  const formsContext = useMemo(() => ({
    forms, setForms, cans, setCans,
  }), [forms, setForms, cans, setCans]);

  return (
    <FormsContext.Provider value={formsContext}>
      {children}
    </FormsContext.Provider>
  );
}

Provider.propTypes = {
  children: PropTypes.node.isRequired,
};
