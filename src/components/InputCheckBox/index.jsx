import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import FormsContext from '../../context/FormsWallContext';
import './index.css';

export default function InputCheckBox({
  id, nameProp, classNameProp, text,
}) {
  const { forms, setForms } = useContext(FormsContext);
  function handleClick({ target }) {
    const { name, className, checked } = target;
    setForms({
      ...forms,
      [name]: {
        ...forms[name],
        [className]: checked,
      },
    });
  }
  return (
    <label htmlFor={id}>
      <input
        data-testid="input-checkBox"
        type="checkbox"
        name={nameProp}
        className={classNameProp}
        onClick={handleClick}
        id={id}
      />
      {text}
    </label>
  );
}

InputCheckBox.propTypes = {
  id: PropTypes.string.isRequired,
  nameProp: PropTypes.string.isRequired,
  classNameProp: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};
