import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function LinkTo({
  path, textProp = '', styleProp = {}, className,
}) {
  return (
    <Link className={className} to={path} style={styleProp}>{textProp}</Link>
  );
}

LinkTo.defaultProps = {
  className: '',
  textProp: '',
  styleProp: {},
};

LinkTo.propTypes = {
  path: PropTypes.string.isRequired,
  textProp: PropTypes.string,
  styleProp: PropTypes.objectOf(PropTypes.string),
  className: PropTypes.string,
};
