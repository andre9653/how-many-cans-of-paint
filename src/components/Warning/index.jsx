import React from 'react';
import PropTypes from 'prop-types';
import LinkTo from '../LinkTo';

export default function Warning({ path, style, text }) {
  return (
    <>
      <h3 className="warning">Ops... Parece que algo deu errado, tente novamente</h3>
      <LinkTo path={path} styleProp={style} textProp={text} />
    </>
  );
}

Warning.propTypes = {
  path: PropTypes.string.isRequired,
  style: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};
