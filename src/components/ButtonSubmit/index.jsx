import React from 'react';
import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function ButtonSubmit({ children, handleClick }) {
  return (
    <Button data-testid="button-submit" size="lg" onClick={handleClick}>{children}</Button>
  );
}

ButtonSubmit.defaultProps = {
  handleClick: () => {},
};

ButtonSubmit.propTypes = {
  children: PropTypes.node.isRequired,
  handleClick: PropTypes.func,
};
