/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable react/jsx-no-bind */
import React, { useContext } from 'react';
import { Container, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import ButtonSubmit from '../ButtonSubmit';
import Wall from '../Wall';
import './index.css';
import FormsContext from '../../context/FormsWallContext';
import { calcM2DoorAndWindow, calcQntPaints, calcQntPaintsByCan } from '../../utils/calculatorQntPaints';

export default function FormsWall() {
  const { forms, setCans } = useContext(FormsContext);
  const navigate = useNavigate();

  function handleClick() {
    let m2 = 0;
    for (const wall in forms) {
      if (!forms[wall].altura || !forms[wall].largura) {
        // eslint-disable-next-line no-undef
        alert('Preencha todos os campos');
        return;
      }
      const wallM2 = calcM2DoorAndWindow(
        forms[wall].altura,
        forms[wall].largura,
        forms[wall].door,
        forms[wall].window,
      );
      if (typeof wallM2 !== 'number') {
        // eslint-disable-next-line no-undef
        alert(wallM2);
        return;
      }
      m2 += wallM2;
    }
    const litersOfPaints = calcQntPaints(m2);
    const result = calcQntPaintsByCan(litersOfPaints);
    setCans(result);
    navigate('/result', { replace: true, state: { cans: result } });
  }

  return (
    <Container>
      <form>
        <Wall wall="1" />
        <Wall wall="2" />
        <Wall wall="3" />
        <Wall wall="4" />
        <Row>
          <ButtonSubmit handleClick={handleClick}>
            Calcular
          </ButtonSubmit>
        </Row>
      </form>
    </Container>
  );
}
