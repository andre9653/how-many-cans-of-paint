import React, { useContext } from 'react';
import {
  Col, Container, FormControl, InputGroup, Row,
} from 'react-bootstrap';
import PropTypes from 'prop-types';
import FormsContext from '../../context/FormsWallContext';
import InputCheckBox from '../InputCheckBox';
import './index.css';

export default function Wall({ wall }) {
  const { forms, setForms } = useContext(FormsContext);

  function handleChange({ target }) {
    const { name, value, classList } = target;
    if (Number.isNaN(Number(value))) {
      return;
    }
    setForms({
      ...forms,
      [name]: {
        ...forms[name],
        [classList[0]]: value,
      },
    });
  }

  return (
    <div className="wall-form">
      <Container>
        <Row>
          <Col>
            <InputCheckBox
              nameProp={`wall${wall}`}
              classNameProp="door"
              text="Porta?"
              id={`door${wall}`}
            />
          </Col>
          <Col>
            <InputCheckBox
              classNameProp="window"
              nameProp={`wall${wall}`}
              text="Janela?"
              id={`window${wall}`}
            />
          </Col>
        </Row>
      </Container>
      <Container>
        <Row>
          <Col>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default" data-testid="input-m2">
                Altura
              </InputGroup.Text>
              <FormControl
                name={`wall${wall}`}
                className="altura"
            // eslint-disable-next-line react/jsx-no-bind
                onChange={handleChange}
                value={forms[`wall${wall}`].altura}
                aria-label="Default"
                data-testid="input-m"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
          </Col>
          <Col>
            <InputGroup className="mb-3">
              <InputGroup.Text id="inputGroup-sizing-default" data-testid="input-m2">
                Largura
              </InputGroup.Text>
              <FormControl
                name={`wall${wall}`}
                className="largura"
            // eslint-disable-next-line react/jsx-no-bind
                onChange={handleChange}
                value={forms[`wall${wall}`].largura}
                aria-label="Default"
                data-testid="input-m"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
          </Col>
        </Row>
      </Container>
      <div className="wall" data-testid="wall">
        <h3>
          Parede
          {' '}
          {wall}
        </h3>
        {forms[`wall${wall}`].door && <div className="door" data-testid="door">Porta</div>}
        {forms[`wall${wall}`].window && <div className="window" data-testid="window">Janela</div>}
      </div>
    </div>
  );
}

Wall.propTypes = {
  wall: PropTypes.string.isRequired,
};
